<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row align-center align-middle">
			<div class="small-12 columns">
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
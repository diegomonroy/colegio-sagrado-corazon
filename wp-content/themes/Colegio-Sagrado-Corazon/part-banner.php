<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'filosofia', 'principios-institucionales', 'mision', 'lema', 'valores-institucionales' ) ) ) : dynamic_sidebar( 'banner_familia_corazonista' ); endif; ?>
				<?php if ( is_page( array( 'pastoral', 'salud', 'orientacion', 'nutricion', 'campana' ) ) ) : dynamic_sidebar( 'banner_servicios' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->